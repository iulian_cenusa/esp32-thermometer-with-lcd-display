//Aug 2020
//Autor: Iulian Cenusa
//----------Configurations----------
#include "config.h"
//----------LCD i2c-----------------
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 8 , 4 );
//--------------DHT22---------------
#include <SimpleDHT.h>
SimpleDHT22 dht22(pinDHT22);
//---------------WIFI---------------
//#include <ESP8266WiFi.h>      //if ESP8266
#include <WiFi.h>               //if ESP32
const char* ssid     = WIFI_SSID;
const char* password = WIFI_PWD;
//---------------MQTT---------------
#include <PubSubClient.h>
WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
int value = 0;
String msg;
String message;
unsigned long now = millis();
//variables for outside tmp/hum
char* t_out = "0.0";
unsigned int cnt = 0;
//--------------Regex---------------
#include <Regexp.h>
// match state object
MatchState ms;
//---------------NTP----------------
const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 10800; //3h
const int   daylightOffset_sec = 0;
//----------------------------------
/*---MQTT callback---*/
void callback(char* topic, byte* payload, unsigned int length) {

  ms.Target ((char*)payload);
  char result = ms.Match (".[0-9].+[0-9]");

  if ( result > 0 ) {

    if (strcmp(topic, "/outside/tmp") == 0 )
    {
      t_out = (char*)payload;
    }
  }

}

//-----------------------------------
/*---MQTT reconnect---*/
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-smart-meter";
    // Attempt to connect
    if (client.connect(clientId.c_str(),MQTT_USER,MQTT_PASS ) ) {
      Serial.println("Connected");
      client.subscribe("/outside/tmp");

      lcd.clear();
      lcd.setCursor(4, 1);
      lcd.print("MQTT OK");
      delay(500);

    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
//----------------------------------
/*---LCD Display Functions---*/
void lcd_display(float t = 0, float h = 0, char* msg = "")
{
  lcd.clear();
  //temperature/humidity indoor
  lcd.setCursor(0, 0);
  lcd.print(msg);
  lcd.setCursor(4, 1);
  if ( t != 0 ) {
    lcd.print("T:");
    lcd.print(t);
    lcd.write(0b11011111);
    lcd.print("C");
  }
  if ( h != 0 ) {
    lcd.setCursor(0, 2);
    lcd.print("U:");
    lcd.print(h);
    lcd.print("%");
  }

}
//----------------------------------
/*---Print_time---*/
void printLocalTime()
{
  struct tm timeinfo;
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Failed to obtain time");
    return;
  }
  Serial.println(&timeinfo, "%D");
  Serial.println(&timeinfo, "%A, %B %d %Y");
  Serial.println(&timeinfo, "%H:%M:%S");
}
//----------------------------------
/*---Print_all---*/
void lcd_test(int x, int y, int z)
{

  struct tm timeinfo;
  if (!getLocalTime(&timeinfo)) {
    Serial.println("Fail Time");
    return;
  }

  lcd.clear();

  //1st row
  lcd.setCursor(0, 0);
  lcd.print("Datetime");
  lcd.setCursor(8, 0);
  lcd.print(" Data");

  //2nd row
  lcd.setCursor(0, 1);
  lcd.print(&timeinfo, "%A");
  lcd.setCursor(8, 1);
  lcd.print(" TO: ");
  lcd.print(z);

  //3rd/4th rows
  lcd.setCursor(16, 0);
  lcd.print(&timeinfo, "%D");
  lcd.setCursor(16, 1);
  lcd.print(&timeinfo, "%H:%M:%S");
  lcd.setCursor(24, 0);
  lcd.print(" TI: ");
  lcd.print(x);
  lcd.setCursor(24, 1);
  lcd.print(" HI: ");
  lcd.print(y);




}
//----------------------------------
//-------------SETUP----------------
//----------------------------------
void setup() {
  Serial.begin(115200);
  pinMode(2, OUTPUT);
  //LCD
  lcd.init();
  lcd.backlight();

  //WIFI
  Serial.println("");
  Serial.println("Connecting to ");
  Serial.print(ssid);
  Serial.println("");
  WiFi.begin(ssid, password);
  lcd.setCursor(0, 0);
  lcd.print("Conn to WIFI");
  lcd.setCursor(0, 1);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    digitalWrite(2, HIGH);
    delay(500);
    digitalWrite(2, LOW);
    delay(500);
  }

  //MQTT
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  //init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  //printLocalTime();

  lcd.clear();

  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  lcd.setCursor(0, 0);
  lcd.print("WIFI OK");
  lcd.setCursor(0, 1);
  lcd.print(WiFi.localIP());

  delay(2000);

}
//----------------------------------
//--------------MAIN----------------
//----------------------------------
void loop() {
  //mqtt
  if (!client.connected()) {
    reconnect();
  }
  client.loop();
  now = millis();

  //DHT22 data read
  int err = SimpleDHTErrSuccess;
  if ((err = dht22.read2(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("Read DHT22 failed, err="); Serial.println(err);
    return;
  }

  if (now - lastMsg > 59000 )
  {
    long rssi = WiFi.RSSI();
    Serial.print("RSSI:");
    Serial.println(rssi);
    lastMsg = now;
    //MQTT publish
    //client.publish("/office/tmp", String((float)temperature).c_str());
    //client.publish("/office/hum", String((float)humidity).c_str());
    message = "{ \"id\": \"DHT22_office_1\" , \"temperature\":" + String(temperature) + " , \"humidity\":" + String(humidity) + " ,\"rssi\":"+ rssi +" }";
    Serial.println( message );
    client.publish("/office", message.c_str());
    delay(25);
  }
  lcd_test((int)round(temperature), (int)round(humidity), atoi(t_out));
  delay(1000);



}
