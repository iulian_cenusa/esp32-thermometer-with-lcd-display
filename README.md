# ESP32 Smart Temperature and Humidity Meter #

This project aims to create a device based on ESP32 microcontroller that reads temperature and humidity from a digital DHT22 senzor but also offer posibility to receive data from other senzors via MQTT and display it on an LCD.

---

## Changelog: ##

### v 0.1 - Display on LCD ###

+ Added base code 
+ Read DHT22 data and display on LCD

### v 0.2 - MQTT  ###

+ Add MQTT code
+ Create different functions to display indoor and outdoor data
+ Regular expressions 

### v 0.3 - Single Display  ###

+ Display all values on one dislay instead of many
+ Remove thingspeak integration
+ Add date and time integration with NTP

### v 0.4 - MQTT enhancements  ###

+ Send data as JSON string
+ Send also RSSI signal

